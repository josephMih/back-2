# RETO TÉCNICO

## Introduccion

Una aplicacion basada en tecnologia java utilizando como framework spring boot y orientada a una arquitectura de microservicios.

## Instrucciones:  
## Ejecución Manual
1: Crear el componente: 
- cd back && mvn clean
- mvn test
- mvn install
- cd ..  
 

## Para el MicroServicio
URL servicio Back:  
localhost:8084 

#
URL Servicio  Con N. Reverse-proy (Puerto 8080):  
localhost:8080/multiply/1/1
 
